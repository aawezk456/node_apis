import * as express from 'express';
import * as mongoose from 'mongoose';
import { callbackify } from 'util';
import { prototype } from 'module';
import { getEnvironmentVariables } from './environments/env';
import {Server}   from './server';

// let app :express.Application = express();

// app.listen( 5000,()=>{
//     console.log('server running at 5000');
// });

let server = new Server().app;

server.listen(5000,()=>{
    console.log('server running at 5000')
});


