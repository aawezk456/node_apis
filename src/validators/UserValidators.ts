import {body} from 'express-validator';

export class UserValidators{
    static login(){
        return [
        body('username','Username is Required').isString(),
        body('email','Email is Required').isEmail(),
        body('password','Password is Required').custom((req)=>{
            if(req.email){
                return true
            }
            else{
                throw new Error('Testing Custom Validation');
            }
        })
    ];
    }
}